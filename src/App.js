import React, { useState, createContext } from 'react';
import Login from './components/Login';
import User from './components/User';


//Context is a collection of states
export const AppContext = createContext(null);

function App() {
  
  const [username, setUsername] = useState("");

  return (
    <AppContext.Provider value={{username, setUsername}}>
      <Login />
      <User  />
    </ AppContext.Provider >
    
  );
};

export default App;
